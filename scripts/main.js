var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('underscore');

/*
  Tactics (main app)
  This will let us make <Tactics/>
*/

var Tactics = React.createClass({

	/* No PropTypes because this is the main app which will be based on the State */

	render: function() {
		return (
			<div className="tactics">
				<Header tagline="Tactics"/>
				<TacticsGame player1={this.props.player1} player2={this.props.player2}/>
			</div>
		)
	}
});

/*
	Header
	<Header />
 */

var Header = React.createClass({

	propTypes: {
		tagline: React.PropTypes.string.isRequired
	},

	render: function() {
		return (
			<header>
				<nav className="navbar navbar-default">
					<div className="container-fluid">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<a className="navbar-brand" href="#">{this.props.tagline}</a>
						</div>
						<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
							<ul className="nav navbar-nav">
								<li className="active"><a href="#">Home (main menu)</a></li>
								<li><a href="#">Settings</a></li>
								<li><a href="#">Help</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</header>
		)
	}
});

/*
	TacticsGame component: is where the actual game will live
	<TacticsGame />
 */

var TacticsGame = React.createClass({

	propTypes: {
		player1: React.PropTypes.object.isRequired,
		player2: React.PropTypes.object.isRequired
	},

	render: function() {
		var points = 56;
		return (
			<div className="container">
				<div className="gamefields">
					<PlayerField player={this.props.player1}/>
					<PlayerTurner />
					<PlayerField player={this.props.player2}/>
				</div>
				<Standings wonPlayer1={this.props.player1.roundsWon} wonPlayer2={this.props.player2.roundsWon}/>
			</div>
		)
	}
});

/*
	PlayerField component: interface for a single player
	<PlayerField />
 */

var PlayerField = React.createClass({

	propTypes: {
		player: React.PropTypes.object.isRequired,
	},

	renderRow: function(item) {
		var score = this.props.player.score;
		return <Row key={item} number={item} hits={score[item].hits} completedByOpponent={score[item].completed}/>
	},

	render: function() {
		var gameClass = 'gamefield';
		if (this.props.player.turn) {
			gameClass += ' turn';
		}
		/* Compute the points sofar for this player */
		var scoreArray = this.props.player.score;
		var points = Object.keys(scoreArray).reduce(function(previousValue, currentValue, currentIndex, array) {
			return previousValue + Math.max(0, scoreArray[currentValue].hits - 3) * currentValue;
		}, 0);
		return (
			<div className={gameClass}>
				<h2>{this.props.player.name} (<span className="player-points">{points}</span>)</h2>
				{_.range(20,9,-1).map(this.renderRow)}
			</div>
		)
	}
});

/*
	Row component: to display a row with players progress in the game for a single number
	<Row />
 */

var Row = React.createClass({

	propTypes: {
		number: React.PropTypes.number.isRequired,
		hits: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired
	},

	render: function() {
		var completed = false;
		var cntrClass = 'tactics-row';
		var status = 'hidden';
		if (this.props.hits >= 3) {
			completed = true;
			cntrClass += ' row-completed';
			status = '';
		}
		var itemClass = 'row-item hit-' + Math.min(3, this.props.hits);

		return (
			<div className={cntrClass}>
				<span className={itemClass} data-item="1">{this.props.number}</span>
				<span className={itemClass} data-item="2">{this.props.number}</span>
				<span className={itemClass} data-item="3">{this.props.number}</span>
				<PointsCounter amount={Math.max(0, this.props.number*(this.props.hits - 3))} status={status} completedByOpponent={this.props.completedByOpponent}/>
			</div>
		)
	}
});

/*
	PointsCounter component: displayed when Row completed (and other player not) to count the points
	<PointsCounter />
 */

var PointsCounter = React.createClass({

	propTypes: {
		status: React.PropTypes.string.isRequired,
		amount: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired
	},

	render: function() {
		var pointsClass = 'points-container row-item ' + this.props.status;
		var button = '';
		if (!this.props.completedByOpponent) {
			button = <button type="button"><span className="glyphicon glyphicon-plus"></span></button>
		}
		var points = <span className="points-amount">{this.props.amount}</span>
		if (this.props.completedByOpponent && this.props.amount === 0) {
			points = '';
		}
		return (
			<span className={pointsClass}>
				{button}
				{points}
			</span>
		)
	}
});

/*
	PlayerTurner component: component to update turns
	<PlayerTurner />
 */

var PlayerTurner = React.createClass({

	render: function() {
		return (
			<div className="turner-container">
				<div className="turner">
					<img src="css/images/HawconsIconsGesturesPng/Black/Filled/@1x/icon-24-one-finger-tap.png"/>
				</div>
			</div>
		)
	}
});

/*
	Standings component: small container for standings in the game
	<Standings />
 */

var Standings = React.createClass({

	propTypes: {
		wonPlayer1: React.PropTypes.number.isRequired,
		wonPlayer2: React.PropTypes.number.isRequired
	},

	render: function() {
		return (
			<div className="standings">
				<h2>{this.props.wonPlayer1} - {this.props.wonPlayer2}</h2>
			</div>
		)
	}
});

var player1 = {
	name: 'Klaas',
	roundsWon: 4,
	turn: false,
	score: {
		'20': {
			hits: 4,
			completed: false
		},
		'19': {
			hits: 3,
			completed: true
		},
		'18': {
			hits: 0,
			completed: true
		},
		'17': {
			hits: 0,
			completed: true
		},
		'16': {
			hits: 1,
			completed: false
		},
		'15': {
			hits: 0,
			completed: true
		},
		'14': {
			hits: 6,
			completed: true
		},
		'13': {
			hits: 4,
			completed: false
		},
		'12': {
			hits: 0,
			completed: true
		},
		'11': {
			hits: 2,
			completed: true
		},
		'10': {
			hits: 3,
			completed: false
		}
	}
};
var player2 = {
	name: 'Jan',
	roundsWon: 1,
	turn: true,
	score: {
		'20': {
			hits: 1,
			completed: true
		},
		'19': {
			hits: 3,
			completed: true
		},
		'18': {
			hits: 5,
			completed: false
		},
		'17': {
			hits: 3,
			completed: false
		},
		'16': {
			hits: 1,
			completed: false
		},
		'15': {
			hits: 3,
			completed: false
		},
		'14': {
			hits: 3,
			completed: true
		},
		'13': {
			hits: 2,
			completed: true
		},
		'12': {
			hits: 4,
			completed: false
		},
		'11': {
			hits: 3,
			completed: false
		},
		'10': {
			hits: 1,
			completed: true
		}
	}
};
ReactDOM.render(<Tactics startTurn="0" player1={player1} player2={player2}/>, document.querySelector('#main'));
